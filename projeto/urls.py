from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.urls import path, include

from blog import views as blog_views

urlpatterns = [

    path('', blog_views.PostListView.as_view(), name='post-list'),
    path('categoria/<int:pk>/', blog_views.CategoryDetailView.as_view(), name='category-detail'),
    path('post/<int:pk>/', blog_views.PostDetailView.as_view(), name='post-detail'),
    path('comentario/<int:post>/', blog_views.CommentCreateView.as_view(), name='comment'),
    path('admin/', admin.site.urls),
    path('usuarios/', include('django.contrib.auth.urls')),
    path('pesquisa/', blog_views.SearchView.as_view(), name='search'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

